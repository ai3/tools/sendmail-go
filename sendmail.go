// A minimal 'sendmail' clone that forwards all email remotely over
// (authenticated) SMTP.
//
package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"net/mail"
	"net/smtp"
	"os"
)

var (
	configPath        = flag.String("C", getenvDefault("SENDMAIL_CONFIG", "/etc/sendmail.json"), "location of the configuration file")
	envelopeSender    = flag.String("f", "", "envelope sender")
	recipientsFromMsg = flag.Bool("t", false, "extract recipients from message")

	// Some common sendmail options that we wish to ignore,
	// luckily the Go flag parser will accept the "short flag"
	// format for them. Ignoring these options makes it less
	// likely that we'll have to modify the existing sendmail
	// commands used inside applications.
	ignoredFlagI   = flag.Bool("i", false, "ignored")
	ignoredFlagM   = flag.Bool("m", false, "ignored")
	ignoredFlagN   = flag.Bool("n", false, "ignored")
	ignoredFlagO   = flag.String("O", "", "ignored")
	ignoredFlagOEE = flag.Bool("oee", false, "ignored")
	ignoredFlagOEM = flag.Bool("oem", false, "ignored")
	ignoredFlagOEP = flag.Bool("oep", false, "ignored")
	ignoredFlagOEQ = flag.Bool("oeq", false, "ignored")
	ignoredFlagOEW = flag.Bool("oew", false, "ignored")
	ignoredFlagOI  = flag.Bool("oi", false, "ignored")
	ignoredFlagOM  = flag.Bool("om", false, "ignored")
	ignoredFlagOO  = flag.Bool("oo", false, "ignored")
	ignoredFlagV   = flag.Bool("v", false, "ignored")

	defaultSMTPServer = "localhost:25"
)

// The configuration is stored in a JSON-encoded file.
type config struct {
	Server string `json:"smtp_server"`
	User   string `json:"smtp_user"`
	Pass   string `json:"smtp_pass"`
}

func readConfig(path string) (server string, auth smtp.Auth, err error) {
	var data []byte
	data, err = ioutil.ReadFile(path)
	if err != nil {
		return
	}

	var config config
	if err = json.Unmarshal(data, &config); err != nil {
		return
	}

	server = config.Server
	if config.User != "" {
		auth = smtp.PlainAuth("", config.User, config.Pass, "")
	}
	return
}

func uniqueRecipients(rcpts []string) []string {
	tmp := make(map[string]struct{})
	for _, r := range rcpts {
		tmp[r] = struct{}{}
	}
	out := make([]string, 0, len(tmp))
	for r := range tmp {
		out = append(out, r)
	}
	return out
}

func getenvDefault(key, dflt string) string {
	if s := os.Getenv(key); s != "" {
		return s
	}
	return dflt
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	smtpServer, smtpAuth, err := readConfig(*configPath)
	if err != nil {
		log.Fatalf("error reading configuration: %v", err)
	}
	if smtpServer == "" {
		smtpServer = defaultSMTPServer
	}

	// Read the whole message from stdin, and parse it.
	msgData, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		log.Fatal(err)
	}
	msg, err := mail.ReadMessage(bytes.NewReader(msgData))
	if err != nil {
		log.Fatal(err)
	}
	header := msg.Header

	// Find envelope sender, from args or from message.
	sender := *envelopeSender
	if sender == "" {
		senderAddr, err := mail.ParseAddress(header.Get("From"))
		if err != nil {
			log.Fatal("could not parse envelope sender")
		}
		sender = senderAddr.Address
	}

	// Find recipients, from args or from message.
	recipients := flag.Args()
	if *recipientsFromMsg {
		for _, rcptHdr := range []string{"To", "Cc", "Bcc"} {
			rcptAddrs, err := header.AddressList(rcptHdr)
			if err != nil && err != mail.ErrHeaderNotPresent {
				log.Fatalf("could not parse recipients: %v", err)
			}
			for _, addr := range rcptAddrs {
				recipients = append(recipients, addr.Address)
			}
		}
	}
	recipients = uniqueRecipients(recipients)
	if len(recipients) == 0 {
		log.Fatal("message has no recipients")
	}

	// Send the message using the net/smtp package.
	err = smtp.SendMail(smtpServer, smtpAuth, sender, recipients, msgData)
	if err != nil {
		log.Fatalf("error sending message: %v", err)
	}
}
