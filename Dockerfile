FROM golang:latest AS build
COPY sendmail.go /sendmail.go
RUN go build -tags netgo -o /sendmail /sendmail.go && strip /sendmail

FROM scratch
COPY --from=build /sendmail /
