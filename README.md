sendmail-go
===

A minimal */usr/sbin/sendmail* drop-in replacement that will accept
emails on standard input and will forward them via a relay host using
(authenticated) SMTP.

# Usage

The tool supports the most common sendmail command-line options. It
will ignore most of them except for *-f* (to set the envelope sender),
and *-t* (to extract recipients from the message itself). If *-t* is
not specified, the tool will expect recipients to be passed as
command-line arguments.

The tool will read a configuration file (default */etc/sendmail.json*,
it can be overridden with the *SENDMAIL_CONFIG* environment variable
or by using the *-C* option) containing relay and authentication
parameters. The configuration file should be encoded in JSON format,
as a dictionary containing the following attributes:

* `smtp_server` server (host:port) to connect to (default
  "localhost:25"). The tool will automatically switch to TLS if
  supported.
* `smtp_user`, `smtp_pass`: credentials for authentication

# Building

You can build a static *sendmail* executable by running:

```shell
$ go build -tags netgo -o sendmail sendmail.go
```

Or reuse our Docker image within a multi-stage Dockerfile:

```
FROM registry.git.autistici.org/ai3/tools/sendmail-go:master AS sendmail

[...]

COPY --from=sendmail /sendmail /usr/sbin/sendmail
```

